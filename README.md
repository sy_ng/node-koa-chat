# node-koa-chat

Chat REST API made with Typescript, NodeJS, Koa, PostgreSQL and TypeORM.
<br>
Db and port configs are loaded from .env.dev file form root dir.

```
#.env.dev file
PORT =
DB_HOST =
DB_PORT =
DB_USERNAME =
DB_PASSWORD =
DB_NAME =
```


## Endpoints

### /user<br>
Handles all users-related logic<br><br>
Supports following request methods:
### POST - for creating a new user<br>

```
Expected request body:
{
    "name": "Awesome User"
}

Response:
{
    "token": "{token}",
    "name": "Awesome User"
}
```

### /message<br>
Handles all messages-related logic<br><br>
Supports following request methods:
### GET - for getting the list of messages<br>
"count" is optional, if count is not provided, returns default number of messages
```
Expected request body:
{
    "token": "{token}",
    "count": "5"
}

Response - an array of objects with such structure
{
    "id": "{message_id}",
    "message": "{message}",
    "author": "{author`s name}",
    "created_at": "{date message created}"
}
```
### POST - for adding a new message<br>
Only registered users can add new messages
```
Expected request body:
{
    "token": "{token}",
    "message": "Your awesome message"
}

Response - a single object representing a new message
{
    "id": "{message_id}",
    "message": "{message}",
    "author": "{author`s name}",
    "created_at": "{date message created}"
}
```

### DELETE - deleting existing message<br>
ID of the message to be deleted should be specified as url parameter<br>
*/message/{message_id}*<br>
Trying to delete someone else's message won't succeed
```
Expected request body:
{
    "token": "{token}"
}

Response - message
{
    "message": "Message deleted successfuly"
}
```

### PATCH - editing existing message<br>
The ID of the message to be edited should be specified as URL parameter<br>
*/message/{message_id}*<br>
Trying to edit someone else's message won't succeed as well
```
Expected request body:
{
    "token": "{token}",
    "message": "Your edited message"
}

Response - an object representing the edited message
{
    "id": "{message_id}",
    "message": "{message}",
    "author": "{author`s name}",
    "created_at": "{date message created}"
}
```