import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import userEntity from './user.entity';
import * as HttpStatus from 'http-status-codes';

const routerOpts: Router.IRouterOptions = {
    prefix: '/user',
}

const router: Router = new Router(routerOpts);

router.post('/', async (ctx: Koa.Context) => {
    if (!ctx.request.body.name) {
        ctx.status = HttpStatus.BAD_REQUEST;
        ctx.body = {
            data: 'No name provided',
        }
        return
    }

    const userRepo: Repository<userEntity> = getRepository(userEntity);
    const existUser: userEntity = await userRepo.findOne({ where: { name: ctx.request.body.name } });
    if (existUser) {
        ctx.status = HttpStatus.INTERNAL_SERVER_ERROR;
        ctx.body = {
            data: 'User with such username already exists',
        }
        return
    }

    const user = userRepo.create(ctx.request.body as Object);
    await userRepo.save(user)
    ctx.body = {
        data: {
            token: user.id,
            name: user.name,
        },
    }
});

export default router;
