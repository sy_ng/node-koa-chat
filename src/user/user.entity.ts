import {Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import Message from "../message/message.entity";

@Entity()
export default class User {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column({unique: true})
    name: string;

    @OneToMany(type => Message, message => message.user)
    messages: Message[];

}