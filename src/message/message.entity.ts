import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import User from '../user/user.entity';

@Entity()
export default class Message {
    
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    message: string;

    @CreateDateColumn()
    created_at: Date;

    @ManyToOne(type => User, (user) => user.messages, {
        eager: true,
        primary: false,
    })
    user: User;
}