import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import messageEntity from './message.entity';
import auth from '../middlewares/middleware.auth';
import * as HttpStatus from 'http-status-codes';

const routerOpts: Router.IRouterOptions = {
    prefix: '/message',
}

const router: Router = new Router(routerOpts);

router.get('/', auth, async (ctx: Koa.Context) => {
    // takes last 'count' messages. Default 'count' is 10
    const messageRepo: Repository<messageEntity> = getRepository(messageEntity);
    const count = ctx.request.body.count ? ctx.request.body.count : 10;

    const messages = await messageRepo.find({
        order: { created_at: 'DESC' },
        take: count,
    });

    ctx.body = messages.map(message => {
        return {
            id: message.id,
            message: message.message,
            author: message.user.name,
            created_at: message.created_at
        }
    });
});

router.post('/', auth, async (ctx: Koa.Context) => {
    if (!ctx.request.body.message) {
        ctx.status = HttpStatus.BAD_REQUEST;
        ctx.body = {
            data: 'No message provided',
        }
        return;
    }

    const messageRepo: Repository<messageEntity> = getRepository(messageEntity);
    const messageData = {
        message: ctx.request.body.message,
        user: ctx.locals.user,
    }
    const message: messageEntity = messageRepo.create(messageData);
    await messageRepo.save(message);
    ctx.body = {
        data: {
            id: message.id,
            message: message.message,
            author: message.user.name,
            created_at: message.created_at
        }
    }
});

router.delete('/:msg_id', auth, async (ctx: Koa.Context) => {
    const messageRepo: Repository<messageEntity> = getRepository(messageEntity);
    const message = await messageRepo.findOne(ctx.params.msg_id);
    console.log(message);
    if (!message) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }
    if (ctx.request.body.token !== message.user.id) {
        ctx.status = HttpStatus.FORBIDDEN;
        ctx.body = {
            data: 'Only deleting your own messages is allowed',
        }
        return;
    }
    await messageRepo.delete(ctx.params.msg_id);
    ctx.body = {
        message: 'Message deleted successfuly',
    }
});

router.patch('/:msg_id', auth, async (ctx: Koa.Context) => {
    const messageRepo: Repository<messageEntity> = getRepository(messageEntity);
    const message = await messageRepo.findOne(ctx.params.msg_id);
    if (!message) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }
    if (ctx.request.body.token !== message.user.id) {
        ctx.status = HttpStatus.FORBIDDEN;
        ctx.body = {
            data: 'Only editing your own messages is allowed',
        }
        return;
    }
    const updateMessage = await messageRepo.merge(message, ctx.request.body);
    messageRepo.save(updateMessage);
    ctx.body = {
        data: { message: updateMessage },
    }
});

export default router;