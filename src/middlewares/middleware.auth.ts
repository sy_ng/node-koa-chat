import * as Koa from 'koa';
import { getRepository, Repository } from 'typeorm';
import userEntity from '../user/user.entity';
import * as HttpStatus from 'http-status-codes';

export default async function auth(ctx: Koa.Context, next: () => Promise<any>) {
    if (!ctx.request.body.token) {
        ctx.status = HttpStatus.BAD_REQUEST;
        ctx.body = {
            data: 'No token provided',
        }
        return
    }

    const userRepo: Repository<userEntity> = getRepository(userEntity);
    const user: userEntity = await userRepo.findOne(ctx.request.body.token);
    if (!user) {
        ctx.status = HttpStatus.UNAUTHORIZED;
        ctx.body = {
            data: 'No user found with such token',
        }
        return
    }

    ctx.locals = { user };
    return next();
}