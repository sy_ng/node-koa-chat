import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as HttpStatus from 'http-status-codes';
import messageController from '../message/message.controller';
import userController from '../user/user.controller';

const app: Koa = new Koa();

app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (error) {
        ctx.status = error.statuscode || error.status || HttpStatus.BAD_REQUEST;
        error.status = ctx.status;
        ctx.body = { error };
        ctx.app.emit('error', error, ctx);
    }
});

app.use(bodyParser());
app.use(userController.routes());
app.use(userController.allowedMethods());
app.use(messageController.routes());
app.use(messageController.allowedMethods());

app.on('error', console.error);

export default app;